package pages.base;


import arp.CucumberArpReport;
import arp.ReportClasses.Step;
import arp.ReportService;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

/**
 * Created by logovskoy on 12/3/2015.
 * Parent class for all that needs to work with webdriver
 */

@ContextConfiguration("classpath:cucumber-context.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public abstract class PageInstance {
    public static WebDriver driver;
    public static PageInstance currentPageObject;

    /**
     * This method checks if previous steps are failed
     *
     * @return result of check
     */
    public static boolean checkIfFurtherStepsAreNeeded() throws TimeoutException, InterruptedException {
        try {
            List<Step> passedSteps = CucumberArpReport.getReport().CurrentTestCase.Steps.stream().filter(p -> p.Actions != null && p.Actions.size() > 0).collect(Collectors.toList());
            if (passedSteps.stream().anyMatch(p -> !p.Status.equals("pass"))) {
                try {
                    ReportService.ReportAction("Step is blocked by further step fails", false);
                    CucumberArpReport.nextStep();
                    return true;
                } catch (Throwable e) {
                }
            }
            waitForJStoLoad();
            return false;
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        waitForJStoLoad();
        return false;
    }

    public static void waitForJStoLoad() throws InterruptedException, TimeoutException {
        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
                } catch (Exception e) {
                    return true;
                }
            }
        };
        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };
        int counter = 0;
        int multiplier=1;
        while (true) {
            if (jQueryLoad.apply(driver) && jsLoad.apply(driver)) {
                return;
            } else {
                counter++;
                multiplier+=multiplier;
                if (multiplier > 29) {
                    throw new TimeoutException("Javascript is taking too long to finish");
                } else {
                    Thread.sleep(1000*multiplier);
                }
            }
        }
    }
}
